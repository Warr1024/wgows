# wgows: WireGuard Over Websockets

Client/server proxy to tunnel wireguard UDP traffic over websockets

### Focused Use-Case:

- Only support UDP for WireGuard; if you want to tunnel something else, do it inside wg
- Rely on tunneled protocol security
- Rely on tunneled protocol reliability (retry and re-handshake), allow some packets to drop
- Rely on reverse proxy to secure websocket sessions against distruption
- Pinger only to detect and GC/restart dead connections

### WebSockets are based on TCP ... isn't it bad to tunnel UDP over TCP?

This system is designed for situations where there is no alternative, e.g. highly restrictive firewalls, like some public hotspots.  Using websockets over HTTPS allows it to work in situations where only TCP ports 80 and 443 outbound are allowed, and even when firewalls are doing deep packet inspection and protocol analysis ... anything short of full TLS MITM.

#### What about TCP Meltdown?

Probably not worth worrying about in practice.  It only seems to happen with borderline networks that are good enough to forward *some* traffic but are unreliable.  I have seen few real-world networks that fall into this category.  Really bad networks will fail to carry reasonable traffic no matter what you do.

Aggressively timing out and disposing of websockets will also help mitigate this.

## Features:

- Simple deployment
  - Standalone, or Docker/Kubernetes
  - Configure via command line or environment variables
- Client
  - Listens on a UDP port, connects to a websocket
  - Forwards received UDP traffic over websocket
  - Forwards return traffic from websocket back to UDP
    - (after having received an inbound UDP packet to know return address)
  - One connection per client
  - Optional direct UDP mode
    - Prefers direct UDP if responses received, websocket maintained as a fallback
    - Good for clients that move between different networks
- Server
  - Listening HTTP server
  - Accepts websocket connections
  - Allocates a UDP port for each connection
  - Forwards websocket messages to a fixed UDP endpoint (wireguard server)
  - Forwards return UDP traffic over websocket
  - Supports multiple connections
    - Limit total connections with early drop
- Symmetric bidirectional keepalive protocol
  - Using protocol messages to avoid relying on socket features
  - Prevent firewalls from timing out connections
  - Allow aggressive connection loss detection
  - Symmetry for simplicity (N.B. both ends must use similar config)

### Unfeatures:

- Multi-socket trunking: could improve recovery time from lossy networks and/or ramp up throughput faster, but the extra complexity and server-side overhead is not worth it.
- Other protocols: focus on wireguard only; other projects exist for e.g. TCP, and other protocols can just be tunneled over wireguard.
- Security/TLS: unneeded complexity; wireguard provides plenty security, and this is expected to be served behind a reverse proxy providing TLS.
- Strong crash-resistance: it's assumed users will competently run in a wrapper, container, or other restarting system.
- Lazy client: it's assumed the use-case for this is only always-open links, so skip the complexity of waiting for a wg client before opening the websocket.
- Multi-process clustering: added complexity, more processes to manage (e.g. non-container use-cases) and no apparent need for that level of scaling.

### Limitations:

- There is potentially considerable overhead when using the websocket tunnel.
  - The traffic may have to traverse multiple additional proxies along the path, in addition to wgows itself (e.g. server reverse proxies) which may increase the total processing cost by an order of magnitude.
  - It's not very good for very bandwidth-intensive operations.
  - Using the direct fallback in the client, when possible, mitigates this significantly.

### Alternatives:

- [erebe/wstunnel](https://github.com/erebe/wstunnel) (Haskell) - Features look good, supports this use-case, but requires a Haskell stack, which I am unlikely to have already installed on most machines, and doesn't seem to support OpenBSD
- [mhzed/wstunnel](https://github.com/mhzed/wstunnel) (nodejs) - Appears to only support TCP
- [flexera-public/wstunnel](https://github.com/flexera-public/wstunnel) (Golang) - Appears to only support TCP

## Building

### Pre-Built Docker Image

- A pre-built [wgows image on Docker Hub](https://hub.docker.com/r/warr1024/wgows) is available for easy deployment.

### Building in Docker

- Run `make build` to build the docker image locally.

### Standalone

- The project can be run directly without containers (e.g. on BSDs that don't have containers), or on Docker Desktop installations on Mac or Windows where host<->container network is a bottleneck.
- Run `npm ci` to install dependencies into the project.

## Deploying

### Docker/Kubernetes

To run the container, you *must* specify either client or server mode on the command line.

Server example:
```sh
  docker run -d --restart=always --name=wgows-server \
    -p <public-web-port>:8080/tcp \
    warr1024/wgows:latest \
    server --wghost=<wireguard-server-address>
```

Client example:
```sh
  docker run -d --restart=always --name=wgows-client \
    -p <wireguard-proxy-address>:<wireguard-proxy-port>:<wireguard-proxy-port>/udp \
    warr1024/wgows:latest \
    client --wghost=0.0.0.0 --wgport=<wireguard-proxy-port> --wsurl=<wss://server-proxy-url> --directhost=<wireguard-host> --directport=<wireguard-port>
```

### Standalone

- To start a server, `node server [options]`.
- To start a client, `node client [options]`.

### Configuration

Common:

- `pingersend`=26.0
  - Send a keepalive message after no traffic has been forwarded for this number of seconds.
  - If using PersistentKeepAlive=25 in wireguard, set this to one second higher (26) to avoid sending any extra traffic under most circumstances.
  - This should be set lower than any intervening firewall timeouts along the path (as wg's own PersistentKeepAlive should be)
- `pingerdie`=30.0
  - If no traffic is received in this amount of time, assume the connection is dead and terminate it.
  - Should be set higher than the remote side's pingersend.

Server:

- `webport`=8080, `webhost`=0.0.0.0
  - Address and port for local HTTP server to listen on.
  - You probably want to set webhost to 127.0.0.1 and run a reverse proxy, especially to add HTTPS capability.
- `wgport`=51820, `wghost`=127.0.0.1
  - Address of the wireguard server to forward all traffic to.
- `maxconn`=100
  - Absolute maximum number of open connections allowed.
  - The server will start to refuse connections probabilistically starting at 50%.
    - Probability 0 below 50%, linearly increases to probability 1 at 100%.
    - Number of actual connections should asymptotically approach maxconn limit.
  - Websocket connections are allowed to complete, but will not forward traffic and will close after a small delay.
    - UDP ports are *not* allocated for these connections.
  - Should help mitigate DoS attacks leading to UDP socket or file descriptor exhaustion.

Client:

- `wgport`=51820, `wghost`=127.0.0.1
  - Port/address to bind to listen for wireguard UDP traffic.
  - Point your wireguard client at this address.
- `wsurl`=ws://127.0.0.1:8080/
  - Websocket URL of listening wgows server to forward wireguard connections to.
- `directport`, `directhost`
  - Optional host:port for direct UDP routing.
    - If set, direct UDP forwarding is attempted before websocket fallback.
    - If not set, no direct UDP is attempted and all traffic uses the websocket.
- `dnsttl`=60, `dnsretry`=5
  - Refresh and retry intervals in seconds for DNS lookup of `directhost`.
  - Has no effect if direct routing is not used.
  - Has no effect if direct host is a dotted quad IP address.