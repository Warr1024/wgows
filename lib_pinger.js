'use strict';

const config = require('./lib_config');
const endconn = require('./lib_endconn');

const resetTimer = (conn, key, delay, func) => {
	const fullkey = `pinger_${key}`;
	if(conn[fullkey])
		clearTimeout(conn[fullkey]);
	if(delay && func)
		conn[fullkey] = setTimeout(() => {
			conn.log({ pinger: key });
			return func();
		}, delay);
	else
		delete conn[fullkey];
};

const end = conn => {
	conn.log({ pinger: 'end' });
	resetTimer(conn, 'send');
	resetTimer(conn, 'die');
};
const send = conn => resetTimer(conn, 'send',
	config.pingersend * 1000,
	() => {
		send(conn);
		if(conn.readyState === 1)
			conn.send(Buffer.alloc(0));
	});
const recv = conn => resetTimer(conn, 'die',
	config.pingerdie * 1000,
	() => {
		end(conn);
		endconn(conn, 1008, `pinger timeout after ${config.pingerdie}s`);
	});
const init = conn => {
	conn.log({ pinger: 'init' });
	send(conn);
	recv(conn);
};

module.exports = { end, send, recv, init };
