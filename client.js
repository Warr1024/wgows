'use strict';

const rethrow = e => {
	throw e;
};
process.on('unhandledRejection', rethrow);

const dgram = require('dgram');
const dnsp = require('dns').promises;
const ws = require('ws');
const log = require('./lib_log');
const config = require('./lib_config');
const pinger = require('./lib_pinger');
const endconn = require('./lib_endconn');
const stats = require('./lib_stats');

let endpoint;
const rinfokeep = { address: true, port: true };
const rinfofilter = rinfo => Object.assign(...Object.entries(rinfo)
	.filter(([k]) => rinfokeep[k])
	.map(([k, v]) => ({
		[k]: v
	})));

const sock = dgram.createSocket('udp4');
let conn;

let connpending;
const reconn = () => {
	stats(null, 'websock', 'connect');
	if(connpending) {
		clearTimeout(connpending);
		connpending = undefined;
	}
	conn = new ws(config.wsurl, { rejectUnauthorized: true, protocol: 'binary' });
	conn.log = log;
	const bail = event => (...err) => {
		stats(null, 'websock', 'disconnect');
		pinger.end(conn);
		endconn(conn, 1011, event, ...err);
		if(!connpending)
			connpending = setTimeout(reconn, 1000 * (Math.random() + 2));
	};
	conn.on('open', () => {
		pinger.init(conn);
		log({ open: true });
	});
	conn.on('error', bail('error'));
	conn.on('close', bail('close'));

	conn.on('message', msg => {
		stats(msg, 'websock', 'recv');
		pinger.recv(conn);
		if(msg.length <= 0)
			return log({ pinger: 'recv' });
		if(!endpoint) return;
		sock.send(msg, endpoint.port, endpoint.address);
	});
};
reconn();

let directmessage;
if(config.directhost && config.directport) {
	let directstate = {};
	const setdirectstate = directstate && (s => {
		if((s.good && 1) !== (directstate.good && 1))
			log({ direct: !!s.good });
		directstate = s;
	});

	const directsock = config.directhost && config.directport && dgram.createSocket('udp4');
	directsock.on('error', rethrow);
	directsock.on('message', msg => {
		stats(msg, 'direct', 'recv');
		if(!endpoint) return;
		setdirectstate({
			good: Date.now() + 2000
		});
		sock.send(msg, endpoint.port, endpoint.address);
	});
	directsock.bind();

	let resolveddirect;
	if(/^\d+\.\d+\.\d+\.\d+$/.test(config.directhost))
		resolveddirect = config.directhost;
	else {
		const resolvecycle = async () => {
			if(directstate.good && directstate.good <= Date.now())
				return setTimeout(resolvecycle, 1000);
			try {
				const found = await dnsp.lookup(config.directhost, 4);
				if(!found || !found.address) throw {found};
				resolveddirect = found.address;
				log({ resolveddirect });
				setTimeout(resolvecycle, config.dnsttl * 1000);
			} catch(err) {
				log({ directdns: err.message || err });
				setTimeout(resolvecycle, config.dnsretry * 1000);
			}
		};
		resolvecycle();
	}

	directmessage = msg => {
		const now = Date.now();
		if(!resolveddirect) return;
		if(!directstate.bad || directstate.bad <= now
			|| conn.readyState !== 1) {
			directsock.send(msg, config.directport, resolveddirect);
			if(!directstate.good || directstate.good <= now)
				setdirectstate({ bad: now + 4000 });
			stats(msg, 'direct', 'send');
			return !directstate.bad;
		}
	};
}

sock.on('error', rethrow);
sock.on('message', (msg, rinfo) => {
	rinfo = rinfofilter(rinfo);
	if(!endpoint || Object.entries(rinfo)
		.find(([k, v]) => endpoint[k] !== v))
		log({ rinfo });
	endpoint = rinfo;
	if(directmessage && directmessage(msg)) return;
	if(conn.readyState !== 1) return;
	stats(msg, 'websock', 'send');
	pinger.send(conn);
	conn.send(msg);
});

sock.bind(config.wgport, config.wghost);
