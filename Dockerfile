FROM alpine:3.17.3

RUN adduser -h /home/user -D user

RUN apk add npm=~9 nodejs=~18

USER user

RUN mkdir -p /home/user/app
WORKDIR /home/user/app

COPY --chown=user *.json .

RUN npm ci -dddd && rm -rf .npm

COPY --chown=user *.js .

RUN mkdir -p /home/user/bin &&\
	for x in client server; do \
	( \
		echo "#!/bin/sh"; \
		echo "while :; do node /app/$x \"\$@\"; sleep 1; done" \
	) >/home/user/bin/$x; \
	done; \
	chmod 0755 /home/user/bin/*

# ----------------------------------------------------------------------

FROM alpine:3.17.3

RUN apk add --no-cache nodejs=~18

COPY --from=0 /home/user/ /

USER nobody

CMD [ "/bin/false" ]
