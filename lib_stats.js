'use strict';

const log = require('./lib_log');

let stats = {};
let pending;

const rpt = () => {
	if(pending)
		clearTimeout(pending);
	pending = undefined;
	log({ stats });
	stats = {};
};

const nav = (msg, obj, ...path) => {
	if(path.length > 0)
		return nav(msg, (obj[path[0]] = (obj[path[0]] || {})), ...path.slice(1));
	obj.count = (obj.count || 0) + 1;
	if(msg && msg.length)
		obj.bytes = (obj.bytes || 0) + msg.length;
	if(!pending)
		pending = setTimeout(rpt, 60 * 1000);
};

module.exports = (msg, ...path) => nav(msg, stats, ...path);
