NAME=warr1024/wgows

build:
	env DOCKER_BUILDKIT=1 docker build -t ${NAME}:latest .

push: build
	docker push ${NAME}:latest