'use strict';

const flatfilter = arr => {
	const out = [];
	for(let i of arr) {
		if(Array.isArray(i))
			out.push(...flatfilter(i));
		else if(i instanceof Buffer) {
			if(i.length > 0) {
				const str = i.toString();
				if(/^[\x09\x0a\x0d\x20-\x7e]*$/.test(str))
					out.push(str);
				else
					out.push(i);
			}
		} else
			out.push(i);
	}
	return out;
};

module.exports = (conn, code, ...reasons) => {
	reasons = flatfilter(reasons);
	conn.log({ code, reasons });
	conn.close(code, `${reasons.map(x => `${x}`).join(' ')}`);
	setTimeout(() => conn.terminate(), 1000);
};
