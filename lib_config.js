'use strict';

const minimist = require('minimist');
const log = require('./lib_log');

const config = minimist(process.argv.slice(2), {
	default: {
		webport: 8080,
		webhost: '0.0.0.0',
		wgport: 51820,
		wghost: '127.0.0.1',
		wsurl: 'ws://127.0.0.1:8080/',
		pingersend: 26,
		pingerdie: 30,
		maxconn: 100,
		dnsttl: 60,
		dnsretry: 5
	}
});

const prefix = `wgows_`;
for(const [k, v] of Object.entries(process.env))
	if(k.startsWith(prefix))
		config[k.substring(prefix.length)] = v;

log({ config });
module.exports = config;
