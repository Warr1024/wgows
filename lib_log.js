'use strict';

module.exports = msg => console.warn(JSON.stringify({
	time: new Date()
		.toISOString(),
	msg
}));
