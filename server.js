'use strict';

const rethrow = e => {
	throw e;
};
process.on('unhandledRejection', rethrow);

const crypto = require('crypto');
const dgram = require('dgram');
const ws = require('ws');
const express = require('express');
const config = require('./lib_config');
const log = require('./lib_log');
const pinger = require('./lib_pinger');
const endconn = require('./lib_endconn');
const stats = require('./lib_stats');

let sockcount = 0;

const wssvr = new ws.Server({
	noServer: true,
	timeout: 10000,
	handleProtocols: p => p.find(x => x === 'binary') || false
});
wssvr.on('error', rethrow);
wssvr.on('connection', (conn, req) => {
	conn.log = req.log;
	conn.log({ connection: 'open' });

	const prob = (sockcount / config.maxconn) * 2 - 1;
	if(prob > 0) {
		const rand = Math.random();
		conn.log({ sockcount, max: config.maxconn, prob, rand, pass: rand >= prob });
		if(rand < prob)
			return setTimeout(() => endconn(conn, 1013,
					`load ${sockcount}/${config.maxconn}`),
				(1 + Math.random()) * 2);
	}
	stats(null, req.reqid, 'start');

	const sock = dgram.createSocket('udp4');
	sockcount++;

	const bail = connection => (...err) => {
		stats(null, req.reqid, 'end');
		sock.close();
		sockcount--;
		pinger.end(conn);
		endconn(conn, 1011, connection, ...err);
	};
	conn.on('error', bail('conn.error'));
	conn.on('close', bail('conn.close'));
	sock.on('error', bail('sock.error'));

	sock.on('message', msg => {
		stats(msg, req.reqid, 'send');
		pinger.send(conn);
		conn.send(msg);
	});
	conn.on('message', msg => {
		stats(msg, req.reqid, 'recv');
		pinger.recv(conn);
		if(msg.length > 0)
			sock.send(msg, config.wgport, config.wghost);
		else
			conn.log({ pinger: 'recv' });
	});

	sock.bind(() => {
		pinger.init(conn);
		conn.log({ bound: (x => x && x.port || x)(sock.address()) });
	});
});

const app = express();
app.use((req, res, next) => req.method === 'get' ?
	next() :
	res.sendStatus(req.url.endsWith('/ping') ? 200 : 404));
const httpsvr = app.listen(config.webport, config.webhost, () => log({
	listening: {
		port: config.webport,
		name: config.webhost
	}
}));
httpsvr.on('upgrade', (req, sock, head) => {
	const reqid =
		`${req.headers && req.headers['x-real-ip'] || '?'}-${new Date().toISOString().replace(/\D/g, '')}-${crypto.randomBytes(4).toString('hex')}`;
	req.reqid = reqid;
	req.log = msg => log({ reqid, msg });

	req.log({ upgrade: 'start' });
	wssvr.handleUpgrade(req, sock, head, upgsock => {
		req.log({ upgrade: 'done' });
		wssvr.emit('connection', upgsock, req);
	});
});
